/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package black.jack;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author istawinski
 */
public class BlackJack {
    
    static Karty[] karty = new Karty[52];
    static Gracz gracz = new Gracz("Użytkownik",9999,10);
    static Gra gra = new Gra();
    
    
    static Stat stat ;
    static Inteli inteli = new Inteli();
    static int tableWidth = 60;
    static int czyjRuch = 1;
    static int kolejka = 0;
    
    /**
     * @param args the command line arguments
     */
    static private void printHexFile(String filename) throws IOException{
    FileInputStream in = null;
        try {
            in = new FileInputStream(filename);
            int read;
            while((read = in.read()) != -1){
                System.out.print(Integer.toHexString(read) + "\t");
            }   } catch (FileNotFoundException ex) {
            Logger.getLogger(BlackJack.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                in.close();
            } catch (IOException ex) {
                Logger.getLogger(BlackJack.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
}
    public static void main(String[] args) throws IOException, InterruptedException {
         stat.stwPlik();
         while(Gracz.portfel> 0){
             kartyInit();
             zerowanie();
             start_gry();
         }
         
         koniec();
     }
    private static void start_gry() throws IOException, InterruptedException
    {
           
           powitanie();
           p("ROZDANIE NR: "+kolejka,0,"S");
           karty = Gra.tasuj(karty);
           pobStawke();
           Gra.rozdajKarty();
           sumuj();
           wypiszStan();
           if(sprCzyKtosWygral()> -1)
           {
               czyjRuch = 3; //gra zakonczona, kolejne rozdanie
           }
           
           
           
           
           
           while(czyjRuch==1)
           {
               
               pyt();
               obslugaOdp(odczytOdp());
               sumuj();
               wypiszStan();
               if(sprCzyKtosWygral()> -1)
           {
               czyjRuch = 3; //gra zakonczona, kolejne rozdanie
           }
               if(sprCzyKtosWygral()> -1)
                {
                    czyjRuch = 3; //gra zakonczona, kolejne rozdanie
                } 
//               inteli.komenda();
//                sumuj();
//                wypiszStan();
//                if(sprCzyKtosWygral()> -1)
//                {
//                    czyjRuch = 3; //gra zakonczona, kolejne rozdanie
//                }
               
                      
           }
           while(czyjRuch == 0)
           {    
                inteli.komenda();
                sumuj();
                wypiszStan();
                if(sprCzyKtosWygral()> -1)
                {
                    czyjRuch = 3; //gra zakonczona, kolejne rozdanie
                }
           }
           
           Thread.sleep(2000);
           nl();nl();nl();nl();nl();nl();
          
    }
    
    static private void printBinaryFile(String filename) throws FileNotFoundException, IOException{
    FileInputStream in = new FileInputStream(filename);
    int read;
    while((read = in.read()) != -1){
        System.out.print(Integer.toBinaryString(read) + "\t");
    }
}
    private static void inteli()
    {
        
    }
    
    
    private static void powitanie()
    {
        rys("*",tableWidth+2);nl();
        p("Witaj w konsolowym BlackJacku",tableWidth,"S");nl();
        System.out.println(stat.ciag);
    }
    private static int sprCzyKtosWygral()
    {
        int odp = sprWynik();
        switch(odp)
        {
            case 1:
                p("WYGRAŁEŚ. Czekaj 5s.",0,"S");
                Gracz.portfel += Gra.pula;
                odp = 1;
                
                
                break;
            case 0:
                p("PRZEGRAŁEŚ. Czekaj 5s.",0,"S");
                odp = 0;
                break;
                
            case 2:
                p("Remis. Wciśnij enter.",0,"S");
                Gracz.portfel += Gra.pula/2;
                odp = 2;
                
                break;
        }
        
        return odp;
}
    private static int sprCzyKtosWygral(int id)
    {
        int odp = rozstrzygnij();
        switch(odp)
        {
            case 1:
                p("WYGRAŁEŚ. Czekaj 5s.",0,"S");
                Gracz.portfel += Gra.pula;
                odp = 1;
                
                
                break;
            case 0:
                p("PRZEGRAŁEŚ. Czekaj 5s.",0,"S");
                odp = 0;
                break;
                
            case 2:
                p("Remis. Wciśnij enter.",0,"S");
                Gracz.portfel += Gra.pula/2;
                odp = 2;
                
                break;
        }
        
        return odp;
}
    private static int sprWynik()
    {
        //System.out.println(Gra.graczSuma+" "+Gra.krupierSuma);
        if(Gra.krupierSuma == 21 && Gra.graczSuma == 21){ return 2;}//remis
        else if( Gra.krupierSuma < 21 && Gra.graczSuma == 21) return 1;//gracz wygral
        else if( Gra.krupierSuma == 21 && Gra.graczSuma < 21) return 0;//krupier wygral
        else if( Gra.krupierSuma > 21 && Gra.graczSuma <= 21) return 1;
        else if( Gra.krupierSuma <= 21 && Gra.graczSuma > 21) return 0;
        //else if( Gra.krupierSuma == 21 && Gra.graczSuma >= 21) return 2;
    
        return -1;
    }
    private static int odp(char c)
    {
        if((int)c == (int)('D') || (int)c == (int)('d')) return 0;
        else if((int)c == (int)('S') || (int)c == (int)('s')) return 1;
        else if(!Gracz.czyDublowal && ((int)c==(int)'P' || (int)c==(int)'p') || (Gracz.portfel >= Gracz.stawka*2)) return 2;
        return -1;
    }
 
    private static int odczytOdp()
    {
    Scanner sc = new Scanner(System.in); 
    char c = sc.next().charAt(0); 
           
                int _odp = odp(c);
                int bom = -1;
                
                if(_odp<0){ System.out.println("PODAJ JEDNĄ Z ODP!"); }
                else
                {
                    switch(_odp)
                    {
                        case 0:
                                 
                                 bom = 0;
                            break;
                        case 1:
                            if(czyjRuch == 1){ 
                               
                                bom =1;
                            }
                            
                            break;
                        case 2:
                            bom = 2;
                            break;
                    }
                } 
                return bom;
    }
    public static void obslugaOdp(int odp) throws IOException
    {
        nl(2);
        //System.out.flush();
        switch(odp)
        {
            case 0:
                if(czyjRuch == 1){ 
                    System.out.println("Gracz dobiera kartę.");}
                else if(czyjRuch == 0)
                {
                    System.out.println("Krupier dobiera kartę.");
                }
                dajKarte();
                break;
            case 1:
                if(czyjRuch == 1){ 
                                System.out.println("Ruch krupiera.");
                                czyjRuch = 0;
                                odslonKarteKrupiera();
                            }
                else  if(czyjRuch == 0){ 
                                System.out.println("Krupier stoi.");
                                rozstrzygnij();
                                czyjRuch = 3;
                                
                            }
                break;
            case 2:
                if(czyjRuch == 0)
                            {
                                Gra.krupierCzyDublowal = true;
                            }
                            else 
                            {
                                Gracz.portfel -= Gracz.stawka;
                                Gracz.czyDublowal = true;
                                Gra.pula *= 2;
                            }
                break;
                
        }
        waitKey();
    }
    
    private static void odslonKarteKrupiera() throws IOException
    {
      stat.zapiszDoStringa(karty[3].symbol);
        karty[3].czyOdslonieta=true;
    }
    
    private static void clr()
    {
        for(int i=0;i<10;i++)
        {
            nl();
        }
    
    }
    private static void kartyInit()
    {
        int symbL = 2;
        String symbol;
        for(int i=0;i<52;i+=4){ 
          for(int j=0;j<4;j++){
            
            symbol = Integer.toString(symbL);
            if(symbL == 11) {
                symbol = "W";
            }else if(symbL == 12){
                symbol = "D";
            }else if(symbL == 13) {
                symbol = "K";
            }else if(symbL == 14) {
                symbol = "A";
            }
            
            String kolor = "";
            if(j == 0) kolor = "♥";
            else if(j == 1) kolor = "♦";
            else if(j == 2) kolor = "♣";
            else if(j == 3) kolor = "♠";
            karty[i+j] = new Karty(symbL,kolor,symbol);
            
           }
          symbL++;
          
         
        }
    }
    
    private static void pyt()
    {
        System.out.println(" D - dobierz") ;
        System.out.println(" S - stój");
        if(!Gracz.czyDublowal && (Gracz.portfel >= Gracz.stawka*2))System.out.println(" P - dubluj stawkę");
    }
    private static void zerowanie()
    {
        kolejka++;
        Gra.pula = 0;
        Gra.zerujKartyGiK();
        Gracz.czyDublowal=false;
        Gra.krupierCzyDublowal = false;
        Gra.krupierSuma=0;
        Gra.graczSuma=0;
        Gra.licznik = 0;
        czyjRuch = 1;
     }
    private static String waitKey()
    {
        String __odp="";
//        System.out.println("## Wcisnij dowolny klawisz... ##");
//          new java.util.Scanner(System.in).nextLine();
          return __odp;
    }
    private static void dajKarte() throws IOException
    {
        if(czyjRuch == 0)
        {
            int ostKart = ileKart(0);
            Gra.kartyKrupiera[ostKart] = Gra.licznik;
        }
        else if(czyjRuch == 1)
        {
            int ostKart = ileKart(1);
            Gra.kartyGracza[ostKart] = Gra.licznik;
        }
       
        
        
        
        karty[Gra.licznik].czyOdslonieta = true;
        Gra.licznik++;
    }
    private static int ileKart(int sw)
    {
        int ost=11;
        for(int i=0;i<11;i++)
        {
            if(sw == 1)
            {
                if(Gra.kartyGracza[i]== -1) return i;
            }else
            {
                if(Gra.kartyKrupiera[i]== -1) return i;
            } 
        }
        return ost;
    }
    private static void wypiszStan()
    {
        
        String daneGracza = "Gracz  : "+Gracz.imie; nl();
        String daneGracza2 ="Kasa   : "+Gracz.portfel+"zł";
        String daneGracza3 ="Stawka : "+Gracz.stawka+"zł";
        String pula =       "Pula   : "+Gra.pula+"zł";
        rys("*",tableWidth+2);nl();
        p(daneGracza,tableWidth);nl();
        p(daneGracza2,tableWidth);nl();
        p(daneGracza3,tableWidth);nl();
        p(pula,tableWidth);nl();
        p(tableWidth,2);
        kartyWys();
        rys("*",tableWidth+2);nl();
        rys("*",tableWidth+2);nl();
     }
    private static int rozstrzygnij()
    {
        if(21 - Gra.graczSuma < 21 - Gra.krupierSuma) return 0;
        else if(21 - Gra.graczSuma > 21 - Gra.krupierSuma) return 1;
        else if(Gra.graczSuma  == Gra.krupierSuma) return 2;
        return 2;
    }
    
    
    private static int sumuj()
    {
        int sumaGracza = 0;
        int sumaKrupiera = 0;
        for(int i=0;i<12;i++){
            if(Gra.kartyGracza[i] != -1 && karty[Gra.kartyGracza[i]].czyOdslonieta){ sumaGracza += karty[Gra.kartyGracza[i]].wartosc;}
        }
        Gra.graczSuma   = sumaGracza;
        for(int i=0;i<12;i++){
            if(Gra.kartyKrupiera[i] != -1 && karty[Gra.kartyKrupiera[i]].czyOdslonieta){ sumaKrupiera += karty[Gra.kartyKrupiera[i]].wartosc;}
        }
        Gra.krupierSuma = sumaKrupiera;
            
        return 0;
    }
    
     private static void pk(String txt,int czyje) // wyswietla dane gracza i krupiera
    {
        String txt2 = (((czyje == 0) ? "   Krupier :" : Gracz.imie+" :"));
        int suma = (((czyje == 0) ? Gra.krupierSuma :Gra.graczSuma ));
        int leng = txt.length()+4+txt2.length()+Integer.toString(suma).length();
        int ileSp = tableWidth - leng ;
        rys("* ");System.out.print(txt2);sp(2,true);System.out.print(suma+"   ");System.out.print(txt);sp(ileSp-4,true);rys(" *");
    } 
     
     
     
     
     
    private static void kartyWys()
    {
        
            int i=0;
            String kartyKrupiera = "";
            
            while(Gra.kartyKrupiera[i]>-1)
            {
                kartyKrupiera += wyswKarte(Gra.kartyKrupiera[i]);
                i++;
            }
           
          pk(kartyKrupiera,0);nl();
          
            i=0;
            String kartyGracza = "";
            
            while(Gra.kartyGracza[i]>-1)
            {
                //System.out.println(wyswKarte(Gra.kartyKrupiera[i]));
                kartyGracza += wyswKarte(Gra.kartyGracza[i]);
                i++;
            }
           
          pk(kartyGracza,1);nl();
        
    }
     
    private static String wyswKarte(int id)
    {
        String kartaSymb = karty[id].symbol;
        String kartaKolor = karty[id].kolor;
        int kartaWart = karty[id].wartosc;
        String sp_ = "";
        if(kartaWart!=10) sp_=" ";
        String karta = "|"+sp_+((karty[id].czyOdslonieta == true)?kartaSymb+kartaKolor:" *" )+" |";
        return karta;
    }
    
     
     
     
     
     
     
    
    
    
    
    
    
    
    
    
    
    
    
    
    
     
     private static void pobStawke()
    {
        Gracz.portfel -= Gracz.stawka;
        Gra.pula += 2 * Gracz.stawka;
    }
    
    
    
    private static void nl()
    {System.out.println();}
     private static void nl(int ile)
    {for(int i=0;i<ile;i++)System.out.println();}
    private static void rys(String zn,int ile)
    {for(int i=0;i<ile;i++) System.out.print(zn);}
    private static void rys(String zn)
    {System.out.print(zn);}
        
    
   
    
    private static void p(String txt,int max)
    {
        int leng = txt.length()+4;
        int ileSp = max - leng+2 ;
        rys("* ");System.out.print(txt);sp(ileSp,true);rys(" *");
    }
   
    
     private static void p(String txt,int max,String allign)
    {
        
        int st = 0;
        int leng = txt.length()+2;
        max = tableWidth - leng;
        int ileSp =  tableWidth - leng ;
        switch(allign){
            case "S":
                st = ileSp/2;
                ileSp = max - leng;
                break;
        }   
        rys("* ");System.out.print(txt);sp(ileSp,true);rys(" *");System.out.println();
    }
    
    
    private static void p(int max,int ile)
    {
        int leng = 4;
        int ileSp = max - leng+2 ;
        for(int i=0;i<ile;i++){ rys("* ");sp(ileSp,true);rys(" *");nl();}
    }
    
     private static void koniec()
    {
     System.out.println("***************************************************************");
       System.out.println("Niezmiernie nam przykro, ale nie masz już środków na koncie :( ");
       System.out.println("Po uzupełnieniu środków przyjdź do nas ponownie.");
       System.out.println("***************************************************************");
        
    }
    
     public static String sp(int ile, boolean sw) {
        String spac = "";
        for (int p = 0; p < ile; p++) {
            if (sw) {
                System.out.print(" ");
            } else {
                spac += " ";
            }
        }
        return spac;
    }

    public BlackJack() throws IOException {
        
        try{
        stat = new Stat();}catch(IOException exD)
        {}
        Stat.stwPlik();
        System.out.println("!!!!!!!!!!!!!!!!!Wczytuje stat!!!!!!!!!!!!");
        Stat.wczytajStat();
    }
    
    
   
}
